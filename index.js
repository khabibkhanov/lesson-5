// Task 1: accepts a string as a parameter and counts the number of vowels within the string

function vowelCount(str1) {
    var vowel_list = 'aeiouyAEIOUY';
    var vcount = 0;
    for(var x = 0; x < str1.length ; x++) {
      if (vowel_list.indexOf(str1[x]) !== -1) {
        vcount += 1;
      }
    }
    return vcount;
  }
  
  console.log(`Task 1: The count of vowels in string: ${vowelCount("The quick browny fox")}`);
  
  // Task 2: calculate number of days left until New year
  
  function showLeftDays(){
      let today = new Date();
      let thisYear = today.getFullYear();
      let newYear = "January 1, " + (thisYear + 1);
      let nextYear = new Date(newYear);
      let msPerDay = 24 * 60 * 60 * 1000 ;  
      let daysLeft = (nextYear.getTime()  - today.getTime()) / msPerDay;
      daysLeft = Math.floor(daysLeft);
      return daysLeft
  }
  
  console.log(`Task 2: The number of days left until new year: ${showLeftDays()}`);
  
  // Task 3: Find Character symbols
  
  function findSymbol(regex, arr) {
      let count = 0
      for(let i = 0; i <= arr.length; i++) {
          // console.log(arr[i]);
          if(regex.indexOf(arr[i] !== -1)) {
              count++
          }
      }
      return count
  }
  
  console.log(`Task 3: Find Character symbols: ${findSymbol("~!@#$%^&*(){}|:?><;", ["Hello# W@ rld", "Br%th#r"])}`);
  
  // Task 4: compute the intersection of two arrays
  
  function intFunc(arr1, arr2) {
      return intResult = arr1.filter(x => arr2.indexOf(x) !== -1);
  }
  
  // console.log(intFunc([1, 3, 5, 8], [1, 2, 3, 5, 9]));
  
  
  // Task 5: Alert Message
  
  function alertMas() {
      setTimeout(function() {
          console.log("Task 5: Alert in 4 seconds");
          // alert("Message")
      }, 4000)
  }
  
  alertMas()
  
  // Task 6: Sum of array 
  
  function sumArr(sum) {
      return sum.reduce((acc, val) => acc + val, 0)
  }
  
  console.log(`Task 6: Sum of array: ${sumArr([3, 4, 6,45 ,30])}`);
  
  // Task 7: Calculate character in string     
  
  function calculateChar(str, char) {
      let count = 0;
  
      for(let i = 0; i<str.length; i++){
          if(str.charAt(i) == char){
              count ++;
          }
      }
      return count
  }
  
  console.log(`Task 7: Total characters: ${calculateChar("Hello World", "l")}`);